

## About
This is the code for the website [https://cosai.gitlab.io/](https://cosai.gitlab.io/), an informational site for the Sloan-funded project, "Collaborating on Software Archiving for Institutions". This site is built using [Nikola](https://getnikola.com/), a static site generator that relies on python3. The instructions for compiling and building the website are below.

## Making the Environment
I would recommend you use a virtualenv to build and view this website. This is a Python tool to create isolated Python environments. The HitchHiker's Guide to Python has a [great guide](http://docs.python-guide.org/en/latest/dev/virtualenvs/) on virtual environments that I used to learn how to use/interact with virtualenvs. 

Here's how to make and activate a virtual environment:
<pre><code># install the tool virtualenv
$ pip install virtualenv

# create the Python 3 virtual environment
$ virtualenv -p python3 venv

# activate the virtual environment
$ source venv/bin/activate
</pre></code>

Now, you can get started and install all of the dependecies of this site in its own environment:

<pre><code># install the dependencies
$ pip install Nikola['extras']
</pre></code>

## Get the Code

You need to then clone the repository so you can make the website! Make sure you have [Git](https://git-scm.com/downloads) installed on your computer before trying these steps.

<pre><code># clone this repo
$ git clone git@gitlab.com:cosai/cosai.gitlab.io.git

# change directory (cd) so you are in the right folder for the website
$ cd cosai.gitlab.io

# build the website
$ nikola build

# see the website
$ nikola serve -b
</pre></code>

### Authoring Blog Posts

All blog posts should go in the `updates` folder. Blog posts should be Text (`.txt`), reStructured Text (`.rst`), Markdown (`.md`), or `.html`. If you'd like to write a post in another format, you'll have to edit the `conf.py` [file](https://gitlab.com/cosai/cosai.gitlab.io/blob/main/conf.py) to add other [accepted input formats](https://getnikola.com/handbook.html#supported-input-formats), starting at [line 126](https://gitlab.com/cosai/cosai.gitlab.io/blob/main/conf.py#L126).

When you author blog posts, you should always have the following metadata at the beginning of the file:

+ Title -- this is what appears on the live post
+ Slug -- the end of the URL to the post. It should have no spaces, only Aa-Zz, 0-9, -, and _
+ Date -- YYYY-MM-DD 
+ Author -- keep your name consistent with every post, please!
+ Link -- this will go to the source link for the post on GitLab (should look like: https://gitlab.com/cosai/cosai.gitlab.io/blob/main/updates/2021-09-01.html)
+ Description -- this is used for SEO purposes, so a small description of the post
+ Type -- Nikola has [two post types](https://getnikola.com/handbook.html#post-types). For our purposes, we'll always use `text`.

If your posts are in HTML, they should look something like this:

```
<!--
.. title: 
.. slug: 
.. date:
.. author: 
.. link: 
.. description: 
.. type: text
-->

<!DOCTYPE html>
<html>
  <body>
  
   <p>Teaser of your post</p>
  
   <img src="/imgs/something.png">

   <!-- TEASER_END -->
   
   <p>Content of your post</p>
   
  </body>
</html>

```

Here is a guide to writing basic HTML: https://www.w3schools.com/html/html_basic.asp

For Markdown, text, and reStructred Text, it should look something like this:

```
.. title: 
.. slug: 
.. date:
.. author: 
.. link: 
.. description: 
.. type: text

Teaser of your post

.. image:: /imgs/something.png

.. TEASER_END

Content of your post

```

There are a few handy guides for writing in these formats as well:

+ Markdown: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
+ reStructured Text: https://github.com/ralsina/rst-cheatsheet/blob/master/rst-cheatsheet.rst

All image files for the blog posts should go into the `imgs` folder. Make sure when you reference your image from the post, you add the `/` before the calling the images folder to help Nikola find it (like in the examples above).

If you don't want to show the complete content of your post in the blog index and RSS feed, you can display just the beginning of them using the `TEASER` magical comment. The Nikola documentation provides a way to do this for each type of post here: https://getnikola.com/handbook.html#teasers. I usually do this to make reading the blog index easier.

## Building and Previewing the Website

When you're done authoring the blog posts and want to preview it locally, build and serve the website the same way you would otherwise: 

```
# build the website
$ nikola build

# browser pops up and shows site
$ nikola serve -b
```

Happy writing!
