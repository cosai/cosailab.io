.. title: Scholarly output
.. slug: scholcomm
.. date: 2022
.. type: text

The scholarly output associated with CoSAI.

.. publication_list:: bibtex/scholcomm.bib
    :style: ieee
    :detail_page_dir:
